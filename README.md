<h1>Differential Equations and Linear Algebra</h1>

Answers and solutions for the book **Differential Equations and Linear Algebra** 2021

by G.B. Gustafson

<h2>Book References</h2>

All PDF sources can be downloaded free [Here](https://gitlab.com/ggustaf/answers/)

<h4>deKindle.pdf</h4>

    Textbook and exercises. Minimum cost paperbacks available at Amazon.

<h4>answersAndSolutions.pdf</h4>

    Current answers and solutions for the book. This PDF is updated when new solutions are available.

<h4>Book Author Contact</h4>

Click on **Issues** to:

-  Request answers or solutions.

 - To contribute a solution, optionally credited to your **alias**.

-  To report errors or corrections for the textbook, exercises or solutions.

-  To send a message or inquiry.

